import numpy as np
from itertools import product

def gen_matrix(expos):
    matrix = []
    primes = [2,3,5,7,13,17,19]
    loop = -1
    for line in expos:
        loop += 1
        matrix.append([])
        for j in range(len(line)):
            matrix[loop].append(primes[loop]**line[j])
    return matrix

def analize_matrix(matrix, lcm_max):
    combin = []
    prev = -1
    loop = -1
    for line in matrix:
        No_elem = 0
        loop += 1
        prev = line[0]
        combin.append([])
        for elem in line:
            if elem != prev:
                combin[loop].append([prev, No_elem])
                prev = elem
                No_elem = 0
            No_elem +=1
        combin[loop].append([prev, No_elem])
    values = []
    loop = 0
    for line in combin:
        values.append([])
        for elem in line:
            values[loop].append( elem[0])
        loop += 1
    combinations = product(*values)
    hyperperiods = []
    for comb in combinations:
        period = 1
        for elem in comb:
            period *= elem
        hyperperiods.append(period)
    return hyperperiods
    
matrix = gen_matrix([[0,0,1],[0,1,2],[0,1,1,2,2,3]])
print(matrix)
print (analize_matrix(matrix, 2))

